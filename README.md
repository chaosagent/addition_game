<!-- Read this document in a markdown viewer. -->
# A Simple Addition Game
A simple addition game using Swing by David Hou for CS170 at Ohlone College.
## Documentation
### Basic Structure
The game is designed using a simple MVC structure. Controllers change the appropriate things in models and views when triggered by a model or view. For each user session, a single Student object is active and used by the controllers. You can change users by logging out and back in again.
### Controllers
#### MainController
This controller initializes all the other controllers and shows the login dialog. It also handles the show scoreboard and logout actions.
#### MainToolbarController
This controller changes the displays in the toolbar when attributes of the current Student model are changed.
#### PlayController
This controller handles the changes that occur during gameplay, including showing new problems and judging answers.
#### ScoreboardController
This controller refreshes the scoreboard when it is shown.
### Interfaces
#### GenericListener
This interface represents a listener that is called by something else when a broadcast is triggered.
### Models
#### AbstractModel
This abstract class provides an interface with implemented methods to add and fire onChange listeners.
#### Scoreboard
This model stores a list of students and processes them to make a scoreboard. When a Scoreboard object is modified, it is saved to disk.
#### Student
This model is just a simple class with getters and setters representing a Student. It supports listeners through extending AbstractModel.
### Util
#### DiskSaver
This class contains two static methods used to write and load Serializable classes from disk.
#### ProblemGenerator
This class generates random addition problems and determines a score value for them.
### Views
#### MainFrame
The main JFrame for this game. contains only mainPanel.
#### MainPanel
The parent panel for everything else. Contains the toolbar, the play area, and the scoreboard. The play area and the scoreboard can be toggled between.
#### MainToolbar
A toolbar containing the scoreboard toggle, information about the current user, and a logout button.
#### PlayPanel
The playing part of the game. Contains a problem view, a answer field, and a submit button.
#### ScoreboardEntryPanel
A panel for each row of the scoreboard. Contains information about the Student it is initialized with.
#### ScoreboardPanel
A scoreboard panel containing a list of ScoreboardEntryPanels. Can redraw scoreboard based on a Scoreboard object.