package interfaces;

public interface GenericListener {
    void onChange(Object triggered);
}
