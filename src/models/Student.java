package models;

import java.io.Serializable;
import java.util.Comparator;

public class Student extends AbstractModel implements Serializable {
    private int rank = -1;
    private String name = "<no name>";
    private int score = 0;

    public Student() { }
    public Student(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        int oldRank = this.rank;
        this.rank = rank;
        if (oldRank != rank) fireOnChangeListeners();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        if (!oldName.equals(name)) fireOnChangeListeners();
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        int oldScore = this.score;
        this.score = score;
        if (oldScore != score) fireOnChangeListeners();
    }

    public void addScore(int n) {
        score += n;
        if (n != 0) fireOnChangeListeners();
    }

    // Compare students by descending score.
    public static class StudentScoreComparator implements Comparator<Student> {
        public int compare(Student a, Student b) {
            return b.getScore() - a.getScore();
        }
    }
}
