package models;

import interfaces.GenericListener;
import util.DiskSaver;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Scoreboard extends AbstractModel implements Serializable {
    private List<Student> students;

    public static final String SAVE_FILE = "scoreboard.dat";

    public Scoreboard() {
        students = new ArrayList<>();
        addListeners();
    }

    public void addListeners() {
        // Save to disk whenever scoreboard changed.
        addOnChangeListener(new GenericListener() {
            @Override
            public void onChange(Object triggered) {
                DiskSaver.saveToDisk((Scoreboard) triggered, SAVE_FILE);
            }
        });
    }

    public void addStudent(Student student) {
        students.add(student);
        student.addOnChangeListener(new GenericListener() {
            @Override
            public void onChange(Object triggered) {
                fireOnChangeListeners();
            }
        });
        sortStudents();
    }

    // Re-order students by descending score and update student ranks.
    public void sortStudents() {
        Collections.sort(students, new Student.StudentScoreComparator());
        for (int i = 0; i < students.size(); i++) {
            students.get(i).setRank(i + 1);
        }
        fireOnChangeListeners();
    }

    public int countStudents() {
        return students.size();
    }

    // Get the top n students in the scoreboard.
    public List<Student> getTopN(int n) {
        List<Student> result = new ArrayList<>();
        for (int i = 0; i < n && i < countStudents(); i++) {
            result.add(students.get(i));
        }
        return result;
    }

    // Find student on scoreboard by name.
    public Student findStudent(String username) {
        for (Student student : students) {
            if (student.getName().equalsIgnoreCase(username)) {
                return student;
            }
        }
        return null;
    }
}
