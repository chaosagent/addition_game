package models;

import interfaces.GenericListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractModel {
    private List<GenericListener> listeners = new ArrayList<>();
    public void addOnChangeListener(GenericListener listener) {
        listeners.add(listener);
    }

    // Call all the listeners in the list
    public void fireOnChangeListeners() {
        for (GenericListener listener : listeners) {
            listener.onChange(this);
        }
    }
}
