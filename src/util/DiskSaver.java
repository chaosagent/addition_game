package util;

import javax.swing.*;
import java.io.*;

public class DiskSaver {
    // Save serializable object to disk using objectOutputStream
    public static void saveToDisk(Serializable object, String saveFile) {
        File outFile = new File(saveFile);
        try {
            outFile.createNewFile(); // Ensure file exists; if it doesn't already exist, create it.
        } catch (IOException e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            JOptionPane.showMessageDialog(null, "An unexpected disk error occurred.\n" + stringWriter.toString());
            System.exit(1); // wat
        }
        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream;
        try {
            fileOutputStream = new FileOutputStream(outFile);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(object);
            objectOutputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            // This should never happen, we just created the file if it wasn't already there.
        } catch (IOException e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            JOptionPane.showMessageDialog(null, "An unexpected disk error occurred.\n" + stringWriter.toString());
            System.exit(1); // wat
        }
    }

    // Read serializable object from disk using objectInputStream
    public static Serializable loadFromDisk(String saveFile) {
        File inFile = new File(saveFile);
        if (!inFile.exists() || !inFile.isFile()) return null;
        FileInputStream fileInputStream;
        ObjectInputStream objectInputStream;
        Serializable result = null;
        try {
            fileInputStream = new FileInputStream(inFile);
            objectInputStream = new ObjectInputStream(fileInputStream);
            result = (Serializable) objectInputStream.readObject();
            fileInputStream.close();
            objectInputStream.close();
        } catch (FileNotFoundException e) {
            // This should never happen
        } catch (IOException e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            JOptionPane.showMessageDialog(null, "An unexpected disk error occurred.\n" + stringWriter.toString());
            System.exit(1); // wat
        } catch(ClassNotFoundException e) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            e.printStackTrace(printWriter);
            JOptionPane.showMessageDialog(null, "An unexpected deserialization occurred.\n" + stringWriter.toString());
            System.exit(1); // wat
        }
        return result;
    }
}
