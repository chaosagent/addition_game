package util;

import java.util.Random;

public class ProblemGenerator {
    private int min, max;
    private String statement = "<none>";
    private int answer = -1;
    private Random random = new Random();
    private int scoreValue;

    public ProblemGenerator(int min, int max) {
        this.min = min;
        this.max = max;
        next();
    }

    // Generate a random addition problem.
    public void next() {
        int a = random.nextInt(max - min + 1) + min;
        int b = random.nextInt(max - min + 1) + min;
        answer = a + b;
        scoreValue = Math.min(Math.abs(a), Math.abs(b));

        statement = String.format("%d + %d = ", a, b);
    }

    public String getStatement() {
        return statement;
    }

    public int getScoreValue() {
        return scoreValue;
    }

    public int getAnswer() {
        return answer;
    }
}
