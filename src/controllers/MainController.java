package controllers;

import models.Scoreboard;
import models.Student;
import util.DiskSaver;
import views.MainPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainController {
    private MainPanel mainPanel;
    private Student currentStudent = new Student("lel");
    private Scoreboard scoreboard;

    private ScoreboardController scoreboardController;
    private PlayController playController;
    private MainToolbarController mainToolbarController;

    public MainController(final MainPanel mainPanel_) {
        this.mainPanel = mainPanel_;

        // Load scoreboard from disk or create a new one if no saved scoreboard
        scoreboard = (Scoreboard) DiskSaver.loadFromDisk(Scoreboard.SAVE_FILE);
        if (scoreboard == null) scoreboard = new Scoreboard();
        else scoreboard.addListeners();

        // Initialize controllers for subpanels
        scoreboardController = new ScoreboardController(mainPanel.getScoreboardPanel(), scoreboard);
        playController = new PlayController(mainPanel.getPlayPanel(), scoreboard);
        mainToolbarController = new MainToolbarController(mainPanel.getMainToolbar());

        // Show login dialog to get a user
        showLoginDialog();

        // Toggle scoreboard and change button text when scoreboard toggle button clicked
        mainPanel.getMainToolbar().addScoreboardButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (mainPanel.toggleScoreboardPanel()) {
                    mainPanel.getMainToolbar().setScoreboardButtonText("Hide scoreboard");
                } else {
                    mainPanel.getMainToolbar().setScoreboardButtonText("Show scoreboard");
                }
            }
        });

        // Show the login dialog when the logout button is clicked.
        mainPanel.getMainToolbar().addLogoutButtonListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                mainPanel.getMainToolbar().setScoreLabelText("Score: - ");
                mainPanel.getMainToolbar().setUserLabelText(" - ");
                showLoginDialog();
            }
        });
    }

    public void showLoginDialog() {
        String username;
        do {
            username = JOptionPane.showInputDialog("Please enter your username:");
            if (username == null) System.exit(0); // If login dialog closed or canceled, exit
        } while (username.equals("") || username.contains("|") || username.contains("\n")); // These are used in serialization.

        // Find student if already exists, else make a new one and add to scoreboard
        currentStudent = scoreboard.findStudent(username);
        if (currentStudent == null) {
            currentStudent = new Student(username);
            scoreboard.addStudent(currentStudent);
        }

        // Update displays with new student info
        setCurrentStudent(currentStudent);
        scoreboard.sortStudents();
        scoreboardController.updateScoreboard();
    }

    public void setCurrentStudent(Student currentStudent) {
        this.currentStudent = currentStudent;
        playController.setStudent(currentStudent);
        mainToolbarController.setStudent(currentStudent);
    }
}
