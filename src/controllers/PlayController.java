package controllers;

import models.Scoreboard;
import models.Student;
import util.ProblemGenerator;
import views.PlayPanel;

import java.awt.event.*;

public class PlayController {
    private Student student;
    private Scoreboard scoreboard;
    private PlayPanel playPanel;
    private ProblemGenerator problemGenerator = new ProblemGenerator(-100, 100);

    public PlayController(PlayPanel playPanel_, Scoreboard scoreboard_) {
        this.scoreboard = scoreboard_;
        this.playPanel = playPanel_;

        // Check answer in text field when submit button clicked
        playPanel.addSubmitActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                submitAnswer();
            }
        });

        playPanel.addAnswerFieldKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    submitAnswer();
                }
            }
        });

        newProblem();
    }

    // Get a new problem and update display
    private void newProblem() {
        problemGenerator.next();
        String statement = problemGenerator.getStatement();
        playPanel.setProblemText(statement);
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void submitAnswer() {
        int givenAnswer;
        try {
            givenAnswer = Integer.parseInt(playPanel.getAnswerText());
        } catch (NumberFormatException e) {
            return;
        }
        if (givenAnswer == problemGenerator.getAnswer()) {
            student.addScore(problemGenerator.getScoreValue());
            newProblem();
        } else {
            student.addScore(-problemGenerator.getScoreValue() * 2);
        }
        playPanel.clearAnswerField();
    }
}
