package controllers;

import interfaces.GenericListener;
import models.Student;
import views.MainToolbar;

public class MainToolbarController {
    private MainToolbar mainToolbar;
    private Student student;

    public MainToolbarController(final MainToolbar mainToolbar_) {
        this.mainToolbar = mainToolbar_;
    }

    public void setUpStudent() {
        mainToolbar.setUserLabelText(student.getName());

        // Update score display when student is changed
        student.addOnChangeListener(new GenericListener() {
            @Override
            public void onChange(Object triggered) {
                mainToolbar.setScoreLabelText("Score: " + String.valueOf(student.getScore()));
            }
        });

        // Force an initial update
        student.fireOnChangeListeners();
    }

    public void setStudent(Student student) {
        this.student = student;
        setUpStudent();
    }
}
