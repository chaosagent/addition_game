package controllers;

import interfaces.GenericListener;
import models.Scoreboard;
import models.Student;
import views.ScoreboardPanel;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class ScoreboardController {
    Scoreboard scoreboard;
    ScoreboardPanel scoreboardPanel;

    public ScoreboardController(ScoreboardPanel scoreboardPanel_, Scoreboard scoreboard_) {
        this.scoreboard = scoreboard_;
        this.scoreboardPanel = scoreboardPanel_;

        // Update scoreboard when shown
        scoreboardPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                updateScoreboard();
            }
        });
    }

    public void updateScoreboard() {
        scoreboard.sortStudents();
        scoreboardPanel.updateScoreboardEntries(scoreboard);
        scoreboardPanel.updateUI();
    }
}
