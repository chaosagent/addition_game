package views;

import models.Student;

import javax.swing.*;
import java.awt.*;

public class ScoreboardEntryPanel extends JPanel {
    private GridLayout gridLayout = new GridLayout(1, 0);
    private JLabel rankLabel = new JLabel(),
            nameLabel = new JLabel(),
            scoreLabel = new JLabel();

    public ScoreboardEntryPanel(Student student) {
        setLayout(gridLayout);

        // Center labels
        rankLabel.setHorizontalAlignment(JLabel.CENTER);
        nameLabel.setHorizontalAlignment(JLabel.CENTER);
        scoreLabel.setHorizontalAlignment(JLabel.CENTER);

        // Populate labels
        rankLabel.setText(String.valueOf(student.getRank()));
        nameLabel.setText(String.valueOf(student.getName()));
        scoreLabel.setText(String.valueOf(student.getScore()));

        // Populate layout
        add(rankLabel);
        add(nameLabel);
        add(scoreLabel);

        // Special background colors for top 3
        switch (student.getRank()) {
            case 1:
                setBackground(new Color(0xFF, 0xD7, 0x00)); // gold
                break;
            case 2:
                setBackground(new Color(0xC0, 0xC0, 0xC0)); // silver
                break;
            case 3:
                setBackground(new Color(0xCD, 0x7F, 0x32)); // bronze
                break;
        }
    }
}
