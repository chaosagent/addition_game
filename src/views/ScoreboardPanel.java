package views;

import models.Scoreboard;
import models.Student;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ScoreboardPanel extends JPanel {
    private GridLayout scoreboardLayout = new GridLayout(0, 1);
    private List<ScoreboardEntryPanel> scoreboardEntries = new ArrayList<>();
    public ScoreboardPanel() {
        setLayout(scoreboardLayout);
    }

    // Refreshes the scoreboard entries
    public void updateScoreboardEntries(Scoreboard scoreboard) {
        for (ScoreboardEntryPanel panel : scoreboardEntries) {
            remove(panel);
        }
        scoreboardEntries.clear();
        for (Student student : scoreboard.getTopN(5)) {
            scoreboardEntries.add(new ScoreboardEntryPanel(student));
        }
        for (ScoreboardEntryPanel panel : scoreboardEntries) {
            add(panel);
        }
        updateUI();
    }
}
