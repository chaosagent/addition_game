package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyListener;

public class PlayPanel extends JPanel {
    private GridLayout gridLayout = new GridLayout(0, 1);
    private JLabel problemLabel = new JLabel();
    private JTextField answerField = new JTextField();
    private JButton submitButton = new JButton("Submit");
    public PlayPanel() {
        setLayout(gridLayout);

        // Format problem and answer field
        problemLabel.setHorizontalAlignment(JLabel.CENTER);
        problemLabel.setFont(new Font(problemLabel.getFont().getName(), Font.PLAIN, 20));

        answerField.setHorizontalAlignment(JTextField.CENTER);

        // Populate layout
        add(problemLabel);
        add(answerField);
        add(submitButton);
    }

    public String getAnswerText() {
        return answerField.getText();
    }

    public void clearAnswerField() {
        answerField.setText("");
    }

    public void addAnswerFieldKeyListener(KeyListener listener) {
        answerField.addKeyListener(listener);
    }

    public void addSubmitActionListener(ActionListener listener) {
        submitButton.addActionListener(listener);
    }

    public void setProblemText(String text) {
        problemLabel.setText(text);
    }
}
