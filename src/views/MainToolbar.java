package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MainToolbar extends JToolBar {
    private GridLayout gridLayout = new GridLayout(1, 0);
    private JButton scoreboardButton = new JButton("Show Scoreboard");
    private JLabel scoreLabel = new JLabel("Score: - "),
            userLabel = new JLabel(" - ");
    private JButton logoutButton = new JButton("Log Out");

    public MainToolbar() {
        setLayout(gridLayout);

        // Center labels
        scoreLabel.setHorizontalAlignment(JLabel.CENTER);
        userLabel.setHorizontalAlignment(JLabel.CENTER);

        add(scoreboardButton);
        add(scoreLabel);
        add(userLabel);
        add(logoutButton);
    }

    public void setScoreLabelText(String text) {
        scoreLabel.setText(text);
    }

    public void setUserLabelText(String text) {
        userLabel.setText(text);
    }

    public void setScoreboardButtonText(String text) {
        scoreboardButton.setText(text);
    }

    public void addScoreboardButtonListener(ActionListener listener) {
        scoreboardButton.addActionListener(listener);
    }

    public void addLogoutButtonListener(ActionListener listener) {
        logoutButton.addActionListener(listener);
    }
}
