package views;

import models.Scoreboard;
import models.Student;

import javax.swing.*;
import java.awt.*;

public class MainPanel extends JPanel {
    private BorderLayout borderLayout = new BorderLayout();
    private CardLayout cardLayout = new CardLayout();
    private MainToolbar mainToolbar = new MainToolbar();
    private JPanel contentPane = new JPanel();
    private PlayPanel playPanel = new PlayPanel();
    private ScoreboardPanel scoreboardPanel = new ScoreboardPanel();

    boolean scoreboardVisible = false;

    public MainPanel() {
        setLayout(borderLayout);

        // Populate layout
        add(mainToolbar, BorderLayout.PAGE_START);

        contentPane.setLayout(cardLayout);
        contentPane.add(playPanel, "playPanel");
        contentPane.add(scoreboardPanel, "scoreboardPanel");

        add(contentPane, BorderLayout.CENTER);

        showPlayPanel();
    }

    public MainToolbar getMainToolbar() {
        return mainToolbar;
    }

    public ScoreboardPanel getScoreboardPanel() {
        return scoreboardPanel;
    }

    public PlayPanel getPlayPanel() {
        return playPanel;
    }

    public void showPlayPanel() {
        cardLayout.show(contentPane, "playPanel");
    }

    public void showScoreboardPanel() {
        cardLayout.show(contentPane, "scoreboardPanel");
    }

    // Toggle scoreboard panel and return new state
    public boolean toggleScoreboardPanel() {
        if (scoreboardVisible) {
            showPlayPanel();
            scoreboardVisible = false;
        } else {
            showScoreboardPanel();
            scoreboardVisible = true;
        }
        return scoreboardVisible;
    }
}
