package views;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFrame extends JFrame {
    private MainPanel mainPanel = new MainPanel();
    public MainFrame() {
        setTitle("Addition Game");
        setSize(500, 500);
        add(mainPanel);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
    }

    public MainPanel getMainPanel() {
        return mainPanel;
    }
}
