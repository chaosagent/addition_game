import controllers.MainController;
import views.MainFrame;

import javax.swing.*;

public class main {
    public static void main(String[] argv) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);

        MainController mainController = new MainController(mainFrame.getMainPanel());
    }
}
